<?php
//print_r(PDO::getAvailableDrivers());

$host="localhost";
$dbname="atomic_project_b37";
$user="root";
$pass="";

try {
# MySQL with PDO_MYSQL
    $DBH = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
    echo "connected <br>";
    $DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

    $data=array('book_title'=>'Book Title 1','author_name'=>'Rahim Ahmed');
    $STHinsert= $DBH->prepare("insert into book_title(book_title,author_name)value (:book_title,:author_name);");
    $STHinsert->execute($data);

    $STHdelete= $DBH->prepare("delete from book_title where book_title='PHP Foundation2';");
    $STHdelete->execute();

    $STHread= $DBH->prepare("select * from book_title");
    $STHread->execute();

    $STHupdate= $DBH->prepare("UPDATE book_title
                            SET book_title='PHP Foundation 2', author_name='Rafique Uddin'
                            WHERE book_title='Book Title 1';");
    $STHupdate->execute();

    $STHread= $DBH->prepare("select * from book_title");
    $STHread->execute();

}
catch(PDOException $e)
{

    echo "I'm sorry, Dave. I'm afraid I can't do that.";
    file_put_contents('PDOErrors.txt', $e->getMessage(), FILE_APPEND);

}

$STHread->setFetchMode(PDO::FETCH_ASSOC);
while($row=$STHread->fetch())
{
    echo $row['id'].'<br>';
    echo $row['book_title'].'<br>';
    echo $row['author_name'].'<br>';
}

/*$STHread->execute();
$STHread->setFetchMode(PDO::FETCH_OBJ);
while($row=$STHread->fetch())
{
    echo $row->id.'<br>';
    echo $row->book_title.'<br>';
    echo $row->author_name.'<br>';
}*/
/*catch(PDOException $e) {
    echo $e->getMessage();
}*/
